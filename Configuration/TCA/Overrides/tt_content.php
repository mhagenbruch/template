<?php
if(!defined('TYPO3_MODE')) {
    die('Access denied');
}

$tempColumns = [
    'cssclass' => [
        'exclude' => 0,
        'label' => 'CSS Klasse',
        'config' => [
            'type' => 'input',
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content','cssclass');
?>