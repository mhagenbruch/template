variables {
    templateName = TEXT
    templateName.stdWrap {
        cObject = TEXT
        cObject {
            data = levelfield:-2, backend_layout_next_level, slide
            override.field = backend_layout
            split {
                token = pagets__
                1.current = 1
                1.wrap = |
            }
        }
        ifEmpty = default
    }
    mainContent < styles.content.get
    mainContent.select.where = colPos = 0

    footerContent < styles.content.get
    footerContent.select.where = colPos = 2
    footerContent.slide = -1

    headerContent < styles.content.get
    headerContent.select.where = colPos = 1
    headerContent.slide = -1

    mainMenu = COA
    mainMenu {
        wrap = <ul class="nav navbar-nav level-1">|</ul>
        10 = HMENU
        10 {
            1 = TMENU
            1 {
                expAll = 1
                NO = 1
                NO {
                    wrapItemAndSub = <li>|</li>
                    stdWrap.htmlSpecialChars = 1
                }

                ACT = 1
                ACT {
                    wrapItemAndSub = <li class="active">|</li>
                }

                IFSUB = 1
                IFSUB {
                    wrapItemAndSub  = <li class="dropdown" >|</li>
                    ATagParams = class="dropdown-toggle"data-toggle="dropdown" data-pageid="{field:uid}"||data-pageid="{field:uid}" class="dropdown-toggle" data-toggle="" role="button" aria-haspopup="true" aria-expand="false"
                    ATagParams.insertData = 1
                    ATagBeforeWrap = 1
                }
                ACTIFSUB = 1
                ACTIFSUB {
                    wrapItemAndSub = <li class="active dropdown">|</li>
                    ATagParams = class="dropdown-toggle" data-toggle="dropdown"||class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expand="false"
                    ATagBeforeWrap = 1
                }
            }
            2 = TMENU
            2 {
                expAll = 1
                wrap = <div class="dropdown-content level-2"><div class="row">|</div></div>
                NO = 1
                NO {
                    wrapItemAndSub = <div class="col-xs-3 menu-item" data-pageid="{field:uid}"><div class="inner">|</div></div>
                    wrapItemAndSub.insertData = 1
                    stdWrap.cObject = COA
                    stdWrap.cObject {
                        10 = TEXT
                        10.field = title
                        10.wrap = <div class="head"><p>|</p></div>
                        20 = FILES
                        20 {
                            references.table = pages
                            references.fieldName = media
                            references.listNum = 0
                            renderObj = IMAGE
                            renderObj.params = class="img-responsive"
                            renderObj.altText.data = file:current:description
                            renderObj.wrap = <div class="img">|</div>
                            renderObj.file {
                                import.data = file:current:publicUrl
                                width = 264
                                height = 190
                            }
                        }
                    }
                }
            }
            3 = TMENU
            3 {
                wrap = <ul class="level-3">|</ul>
                NO = 1
                NO {
                    wrapItemAndSub = <li>|</li>
                    stdWrap.cObject = COA
                    stdWrap.cObject {
                        10 = TEXT
                        10.field = title
                        10.wrap = <p>|</p>
                        20 = TEXT
                        20.field = subtitle
                        20.wrap = <span>|</span>
                    }
                }
            }
        }
    }
    mobileMenu = COA
    mobileMenu {
        wrap = <ul class="nav navbar-nav">|</ul>
        10 = HMENU
        10 {
            1 = TMENU
            1 {
                expAll = 1
                NO = 1
                NO {
                    wrapItemAndSub = <li>|</li>
                    stdWrap.htmlSpecialChars = 1
                }

                ACT = 1
                ACT {
                    wrapItemAndSub = <li class="active">|</li>
                }

                IFSUB = 1
                IFSUB {
                    wrapItemAndSub  = <li class="dropdown" >|</li>
                    ATagParams = class="dropdown-toggle"data-toggle="dropdown" data-pageid="{field:uid}"||data-pageid="{field:uid}" class="dropdown-toggle" data-toggle="" role="button" aria-haspopup="true" aria-expand="false"
                    ATagParams.insertData = 1
                    ATagBeforeWrap = 1
                }
                ACTIFSUB = 1
                ACTIFSUB {
                    wrapItemAndSub = <li class="active dropdown">|</li>
                    ATagParams = class="dropdown-toggle" data-toggle="dropdown"||class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expand="false"
                    ATagBeforeWrap = 1
                }
            }
            
        }
    }
    mainMenuIpad = COA
    mainMenuIpad {
        wrap = <ul class="nav navbar-nav">|</ul>
        10 = HMENU
        10 {
            1 = TMENU
            1{
            }
        }
    }

    langMenu = HMENU
    langMenu {
        special = language
        special.value = 0,1
        special.normalWhenNoLanguage = 0
        wrap = <ul id="language-select"> | </ul>
        1 = TMENU
        1 {
            noBlur = 1
            NO = 1
            NO {
                linkWrap = <li>|</li>
                stdWrap.override = DE || EN
                doNotLinkIt = 1
                stdWrap.typolink.parameter.data = page:uid
                stdWrap.typolink.additionalParams = &L=0 || &L=1
                stdWrap.typolink.addQueryString = 1
                stdWrap.typolink.addQueryString.exclude = L,id,cHash,no_cache
                stdWrap.typolink.addQueryString.method = GET
                stdWrap.typolink.useCacheHash = 1
                stdWrap.typolink.no_cache = 0
            }
            ACT < .NO
            ACT.linkWrap = <li class="active not-shown">|</li>

            USERDEF1 < .NO
            USERDEF2 < . ACT
        }
    }
    footerMenu = HMENU
    footerMenu {
        special = directory
        special.value = 93
        wrap = <div>|</div>
        1 = TMENU
        1 {
            expAll = 1
            NO = 1
            NO {
                wrapItemAndSub = |<span>&nbsp;&#124;&nbsp;</span> |*| |<span>&nbsp;&#124;&nbsp</span> |*| |
            }
        }
    }


    copyright = COA
    copyright.10 = TEXT
    copyright.10 {
        data = date:U
        strftime = %Y
        wrap = &copy; | Hasenauer+Koch GmbH + Co. KG
    }
    copyright.20 = TEXT
    copyright.20.typolink.parameter = 34
    copyright.20.wrap = &nbsp;&#124;&nbsp; |
    copyright.20.noTrimWrap = &#124; |

    copyright.30 = TEXT
    copyright.30.typolink.parameter = 7
    copyright.30.wrap = &nbsp;&#124;&nbsp; |
    copyright.30.noTrimWrap = &#124; |

    searchbox = COA_INT
    searchbox {
        10 < plugin.tx_kesearch_pi1
        10.resultPage = 8
        #10.cssFile >
        #wrap = <div class="pull-right">|</div>
    }

    headerimage = IMAGE
    headerimage {
        file {
            import.data = levelmedia:-1, slide
            treatIdAsReference = 1
            import.listNum = 0
        }
        params = class="header-image img-responsive"
    }

    link = TEXT
    link.value = test



}