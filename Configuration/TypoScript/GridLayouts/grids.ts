# Ausgabe der Grid Elemente anpassen
tt_content.gridelements_pi1.20.10.setup {
    # container
    container < lib.gridelements.defaultGridSetup
    container {
        wrap = <div class="container">|</div>
    }
    container-fluid < lib.gridelements.defaultGridSetup
    container-fluid {
        wrap = <div class="container-fluid">|</div>
    }
    1spalte < lib.gridelements.defaultGridSetup
    1spalte {
        wrap = <div class="row">|</div>
        columns {
            102 < .default
            102.wrap = <div class="col-xs-12">|</div>
        }
    }
    2spalten < lib.gridelements.defaultGridSetup
    2spalten {
        wrap = <div class="row">|</div>
        columns {
            103 < .default
            103.wrap = <div class="col-xs-12 col-sm-6">|</div>
            104 < .default
            104.wrap = <div class="col-xs-12 col-sm-6">|</div>
        }
    }
    3spalten < lib.gridelements.defaultGridSetup
    3spalten {
        wrap = <div class="row">|</div>
        columns {
            105 < .default
            105.wrap = <div class="col-xs-4">|</div>
            106 < .default
            106.wrap = <div class="col-xs-4">|</div>
            107 < .default
            107.wrap = <div class="col-xs-4">|</div>
        }
    }
    4spalten < lib.gridelements.defaultGridSetup
    4spalten {
        wrap = <div class="row">|</div>
        columns {
            108 < .default
            108.wrap = <div class="col-xs-3">|</div>
            109 < .default
            109.wrap = <div class="col-xs-3">|</div>
            110 < .default
            110.wrap = <div class="col-xs-3">|</div>
            111 < .default
            111.wrap = <div class="col-xs-3">|</div>
        }
    }
    2spalten9-3 < lib.gridelements.defaultGridSetup
    2spalten9-3 {
        wrap = <div class="row">|</div>
        columns {
            113 < .default
            113.wrap = <div class="col-xs-12 col-sm-9">|</div>
            114 < .default
            114.wrap = <div class="col-xs-12 col-sm-3">|</div>
        }
    }
    lightGreenContainer < lib.gridelements.defaultGridSetup
    lightGreenContainer {
        wrap = <div class="lightGreenContainer"><div class="container">|</div></div>
    }
    2spalten5-2 < lib.gridelements.defaultGridSetup
    2spalten5-2 {
        columns {
            115 < .default
            115.wrap = <div class="col-xs-12 col-sm-6 col-lg-5 col-xl-5">|</div>
            116 < .default
            116.wrap = <div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1">|</div>
        }
        wrap = <div class="row">|</div>
        # other wrapping for layout classes
        wrap.cObject = CASE
        wrap.cObject {
            key.field = layout
            default = TEXT
            default.value = <div class="row mydefaultclass">|</div>

            2 = TEXT
            2.value = <div class="row brown">|</div>

            3 = TEXT
            3.value = <div class="row green">|</div>
        }
    }
}
