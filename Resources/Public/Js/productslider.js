(function($) {
    var slider = {
        'settings' : {
            arrows: true
        },
        /**
         * slider container
         */
        'slider' : null,

        /**
         * ul of the slider
         */
        'ul' : null,

        /**
         * elements of this slider
         */
        'elements' : [],

        /**
         * current position of the slider
         */
        'current_index' : 0,

        /**
         * timer for auto scroll
         */
        'timer' : null,

        /**
         * interval for scrolling (ms)
         */
        'interval' : 5000,

        /**
         * scroll direction, -1 or +1
         */
        'direction' : +1,

        /**
         * clean whitespace between <li> elements
         */
        'cleanWhitespace' : function() {
            var whiteSpaceNodes = $(this).contents().filter(function() {
                return this.nodeType == 3;
            });
            whiteSpaceNodes.remove();
        },

        /**
         * init the slider
         */
        'init' : function() {
            if ($(this).filter('div').length != 1) {
                return false; // wrong element
            }
            if ($(this).children().filter('ul').length != 1) {
                return false; // wrong element
            }

            slider.slider = $(this);
            slider.ul = $('ul', slider.slider);
            $("li", slider.ul).each(slider.init_image);
            /*if(slider.ul.find('img').length <= 1 ) {
                return false;
            }*/
            slider.slider.addClass('slider-base');
            slider.cleanWhitespace.call(slider.ul);


            if (slider.elements.length > 0) {
                slider.elements[slider.current_index].li.css('width', '100%');
            }
            slider.addArrows.call(this);
            slider.addCircles.call(this);
            $('.slider-base').bind("swipeleft", slider.slideToLeft);
            $('.slider-base').bind("swiperight", slider.slideToRight);

            return true;
        },

        /**
         * init a single image of the slider
         */
        'init_image' : function() {
            /*if(slider.ul.find('img').length <= 1) {
                return false;
            }*/
            var li = $(this);
            var img = $('img', this);
            var url = img.attr('src');
            var div = $('<div class="image">');

            img.remove();
            li.prepend(div);

            div.css('background-image', 'url('+url+')');
            if(li.find('.text').length) {
                text = li.find('.text');
                clone = text.clone(true);
                div.append(clone)
                text.remove();
            }
            slider.elements.push({
                'url': url,
                'li' : li,
                'div' : div
            });

            li.css('width', 0);
        },

        /**
         * init the timer function
         */
        'init_timer' : function() {
            slider.timer = window.setInterval(slider.slide, slider.interval);
        },
        /**
         * slide the slider
         */
        'slide' : function(direction) {
            slider.direction = direction;
            for(var i = 0; i < slider.elements.length; i++) {
                slider.elements[i].li.removeClass('slider-animate-out');
                slider.elements[i].li.removeClass('slider-animate-in');
            }

            var current_li = slider.elements[slider.current_index].li;

            if (slider.direction > 0) {
                slider.current_index++;
                if (slider.current_index >= slider.elements.length) {
                    slider.current_index = 0;
                }
            }
            else if (slider.direction < 0) {
                slider.current_index--;
                if (slider.current_index < 0) {
                    slider.current_index = slider.elements.length - 1;
                }
            }
            else {
                return; // no direction is specified...
            }
            var next_li = slider.elements[slider.current_index].li;

            if (slider.direction > 0) {
                current_li.width(0);
                current_li.addClass('slider-animate-out');
                next_li.addClass('slider-animate-in');
                next_li.width("100%");
            }
            if (slider.direction < 0) {
                current_li.width(0);
                current_li.addClass('slider-animate-out');
                next_li.addClass('slider-animate-in');
                next_li.width('100%');
            }
            window.setTimeout(function() {
                $('.circle').removeClass('active');
                $('.circle[data-image="'+slider.current_index+'"]').addClass('active')
            }, 1000);
        },
        'addArrows' : function() {
            if(window.innerWidth >= 1024 && slider.elements.length > 1) {
                $('.slider-base').append('<div class="left"></div><div class="right"></div>');
                sliderHeight = parseInt($('.slider-base').height());
                arrowHeight = parseInt($('.left').height());
                $('.left, .right').css('top', (sliderHeight - arrowHeight) / 2 + 'px');
                $('.left').bind('click', slider.slideToLeft);
                $('.right').bind('click', slider.slideToRight);
            }
        },
        'addCircles': function() {
            if(slider.settings.showCircles) {
                $('.slider-base').append('<div class="circles"></div>');
                for(var i=0; i < slider.elements.length; i++) {
                    if(i==0) {
                        $('.circles').append('<div class="circle active" data-image="' + i + '"></div>');
                    } else {
                        $('.circles').append('<div class="circle" data-image="' + i + '"></div>');
                    }
                }
                $('.circles').css('bottom', slider.settings.circlesBottom + 'px')
            }
        },
        'slideToLeft' : function() {
            if(slider.current_index < slider.elements.length-1) {
                slider.slide.call(this, 1)
            }
        },
        'slideToRight' : function() {
            if(slider.current_index > 0) {
                slider.slide.call(this, -1)
            }
        }
    };

    $.fn.slider = function(options, action) {
        slider.settings = $.extend({
            showCircles: true,
            circlesBottom: 17
        }, options);
        if (action == undefined) {
            return slider.init.call(this)
        }
    }
})(jQuery)

$(function() {
    //console.debug("return value: " + $("#slider").slider());
    $('#width').on('input', function(event) {
        $('#container').css('width', $(this).val() + 'px');
    });
    $('#height').on('input', function(event) {
        $('#container').css('height', $(this).val() + 'px');
    });
});